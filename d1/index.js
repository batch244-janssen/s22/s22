// console.log("Happy Monthsary, Batch 244!")

// Array Methods
    /*
        Allow us to manipulate and access array items/elements
        1. Mutator Methods
        2. Non-mutator Methods
        3. Iteration Methods
    */

// [SECTION] Mutator Methods
    /*
        - Mutator methods are functions that "mutate" or change an array after they are created
    */
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

    // push()
    /*
        Adds an element at the end of an array AND returns the array's length
    */
    console.log("Current fruits array:");
    console.log(fruits);

    let fruitsLength = fruits.push("Mango");
    console.log(fruitsLength);//5

    console.log("Mutated array from push method")
    console.log(fruits);//new array

    // Adding multiple elements to an array
    fruits.push("Avocado", "Guava")
    console.log("Mutated array from push method with multiple elements:")
    console.log(fruits);

    // pop()
    /*
        - Removes the last element in an array AND returns the removed element
        Syntax:
        arrayName.pop()
    */

    let removedFruit = fruits.pop();
    console.log(removedFruit);//Guava

    console.log("Mutated array from pop method:");
    console.log(fruits);

    // unshift()
    /*
        Adds one or more elements at the beginning of an array AND returns the array length
    */

    let fruitsUnshift = fruits.unshift("Lime", "Banana");
    console.log(fruitsUnshift);
    console.log("Mutated array from unshift method:");
    console.log(fruits);

    // shift()

    /*
        - Removes an element at the beginning of an array AND returns the removed element
    */

    let anotherFruit = fruits.shift();
    console.log(anotherFruit);

    console.log("Mutated array from shift method:");
    console.log(fruits);

    // splice()

    /*
        - Simultaneously removes elements from a specified index number and adds new elements
        - Returns removed element/s
        Syntax: arrayName.splice(startingIndex, deleteCount, elementsToBaAdded);
    */

    let fruitsSplice = fruits.splice(0, 3, "Lime", "Cherry");
    console.log(fruitsSplice);

    console.log("Mutated array from splice method:");
    console.log(fruits);

    // sort()
    /*
        - Rearranges the array elements in alphabetcal order
    */
    fruits.sort();
    console.log("Mutated array from sort method");
    console.log(fruits);

    // reverse()
    /*
        - Reverses the order of the elements in the array
    */
    fruits.reverse();
    console.log("Mutated array from reverse method:");
    console.log(fruits);



// [SECTION] Non-Mutator Methods
    /*
        Non-Mutator methods are function that do not modify or change an array after they are created.
    */

    let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
    console.log(countries);

    // indexOf()
    /*
        - Returns the index number of the first matching element found in an array.
        - If no match was found, the result will be -1.
        - The search process will be done from index 0 to the last element
    */

    let firstIndex = countries.indexOf("PH", 2);
    console.log("Result of indexOf method: " + firstIndex);

    let invalidCountry = countries.indexOf("BR");
    console.log("Result of indexOf method: " + invalidCountry);

    // lastIndexOf()
    /*
        - Returns the index number of the last matching element found in an array.
        - The search process will start from the last element going to the first element.
        Syntax:
        arrayName.lastIndexOf(searchValue, fromIndex)
    */

    // Getting the index number starting from the last element
    let lastIndex = countries.lastIndexOf("PH");
    console.log("Result of lastIndexOf method: " + lastIndex);

    // Getting the index number starting from a specified index
    let lastIndexStart = countries.lastIndexOf("PH", 4);
    console.log("Result of lastIndexOf method: " + lastIndexStart);
   

    // slice()

    /*
        Slices/portions elements from an array and returns a new array
        Syntax:
            arrayName.slice(startingIndex);
            arrayName.slice(startingIndex, endingIndex);

    */

    // Slicing off elements from a specified index to the last element
    let slicedArrayA = countries.slice(2);
    console.log("Result from slice method: ");
    console.log(slicedArrayA);

    // Slicing off elements from a specified index to another index
    let slicedArrayB = countries.slice(1, 5);
    console.log("Result from slice method: ");
    console.log(slicedArrayB);

    // Slicing off elements starting from the last element of the array
    let slicedArrayC = countries.slice(-3);
    console.log("Result from slice method: ");
    console.log(slicedArrayC);

    // toString()
    /*
        - Returns an array as a string separated by commas
        Syntax:
        arrayName.toString()
    */

    let stringArray = countries.toString();
    console.log("Result from toString method: ");
    console.log(stringArray);

    // concat()
    /*
        Combines two arrays and returns the combined result
        Syntax:
        arrayA.concat(arrayB)
    */

    let tasksArrayA = ["drink html", "eat javascript"];
    let tasksArrayB = ["inhale css", "breathe bootstrap"];
    let tasksArrayC = ["get git", "be node"];

    let tasks = tasksArrayA.concat(tasksArrayB);
    console.log("Result from concat method: ");
    console.log(tasks);

    // Combining multiple arrays
    let allTasks = tasksArrayA.concat(tasksArrayB,tasksArrayC);
    console.log(allTasks);

    // Combining arrays with elements
    let combinedTasks = tasksArrayA.concat("smell express", "throw react");
    console.log("Result from concat method: ");
    console.log(combinedTasks);
    console.log(tasksArrayA);

    // join()
    /*
       - Returns an array as a string separated by specified separator string
       - Syntax:
            arrayName.join("separatorString");
    */

    let users = ["John", "Jane", "Joe", "Robert"];
    console.log(users.join());
    console.log(users.join(" "));
    console.log(users.join("-"))


// [SECTION] Iteration Methods
    /*
        - Iteration methods are lops designed to perform repetitive tasks on arrays
        - Iteration methods loop over all items in an array
        - Array iteration methods normally work with a function supplied as an argument
    */

    // forEach()
    /*
        - Iterates on each array element
        - For each item in the array, the anonymous function passed in the forEach() method will be run.
        - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.

        Syntax:
        arrayName.forEach(function(individualElement) {
            statement/code block
        })
    */

    allTasks.forEach(function(task) {
        console.log(task);
    })

    // Using forEach with conditional statements
    let filteredTasks = [];

    allTasks.forEach(function(task) {
        console.log(task)
            // If the element/string's length is greater than 10 characters
        if(task.length > 10) {

            // Add the element to the filteredTasks array
            filteredTasks.push(task);
        }
    });
    console.log("Result of filtered tasks: ");
    console.log(filteredTasks);

    // map()
    /*  
        - Iterates on each element AND returns a new array
        - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
        let/const resultArray = arrayName.map(function(indivElement))
    */

    let numbers = [1, 2, 3, 4, 5];
    let numberMap = numbers.map(function(number) {
    return number * number;
    });
    console.log("Original array: ");
    console.log(numbers);//Original is unaffected by map()

    console.log("Result of map method:");
    console.log(numberMap);//A new array is returned by map() and stored in the variable.


    //map() vs forEach()

    let numberForEach = numbers.forEach(function(number){

        return number * number

    })

    console.log(numberForEach);//undefined. 

    //forEach(), loops over all items in the array as does map(), but forEach() does not return a new array.

    // every()
    /*
        - Checks if all elements in an array meet the given condition
        - Returns a true value if all elements meet the condition and false if otherwise
        Syntax:
        let/const resultArray = arrayName.every(function(indivElement) {
            return expression/consition
        })
    */

    let allValid = numbers.every(function(number) {
        return (number < 3);
    });
    console.log("Result of every method: ");
    console.log(allValid);//false

    // some()

    /*
        Checks if at least 1 element in the array meets the given condition

        - Syntax
            let/const resultArray = arrayName.some(function(indivElement) {
                return expression/condition;
            })
    */

    let someValid = numbers.some(function(number) {
        return (number < 2);
    });
    console.log("Result of some method:");
    console.log(someValid);//true

    // filter()

    let filterValid = numbers.filter(function(number) {
        return (number < 3);
    });
    console.log("Result from filter method:");
    console.log(filterValid);

    let nothingFound = numbers.filter(function(number) {
        return (number === 0);
    })
    console.log(nothingFound);//empty array


    //includes()
        /* 
            - Evaluates elements from left to right and returns/reduces the array into a single value
            - Syntax
                let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
                    return expression/operation
                })
            - The "accumulator" parameter in the function stores the result for every iteration of the loop
            - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
            - How the "reduce" method works
                1. The first/result element in the array is stored in the "accumulator" parameter
                2. The second/next element in the array is stored in the "currentValue" parameter
                3. An operation is performed on the two elements
                4. The loop repeats step 1-3 until all elements have been worked on
        */

    let products = ["Mouse", "Keyboard", "Laptop", "Monitor"]

    let productFound1 = products.includes("Mouse");
    console.log(productFound1);//true

    let productFound2 = products.includes("Headset");
    console.log(productFound2); //false

    /*
        - Methods can be "chained" using them one after another
        - The result of the first method is used on the second method until all "chained" methods have been resolved
        - How chaining resolves in our example:
            1. The "product" element will be converted into all lowercase letters
            2. The resulting lowercased string is used in the "includes" method
    */
    let filteredProducts = products.filter(function(product) {
        return product.toLowerCase().includes("a");
    });

    console.log(filteredProducts);

    // reduce()
    console.log(numbers);
    let total = numbers.reduce(function(x, y) {
        console.log("This is the value of x: " + x);
        console.log("This is the value of y: " + y);
        return x + y;
    })
    console.log(total);

    let iteration = 0;

    let reducedArray = numbers.reduce(function(x, y) {
        // Used to track the current iteration count and accumulator/currentValue data
        console.warn('current iteration: ' + ++iteration);
        console.log('accumulator: ' + x);
        console.log('currentValue: ' + y);

        // The operation to reduce the array into a single value
        return x + y;
    });
    console.log("Result of reduce method: " + reducedArray);

    // reducing string arrays
    let list = ['Hello', 'Again', 'World'];

    let reducedJoin = list.reduce(function(x, y) {
        return x + ' ' + y;
    });
    console.log("Result of reduce method: " + reducedJoin);

